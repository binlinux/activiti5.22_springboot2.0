package com.srkj.controller;

import com.srkj.entity.Sys_user;
import com.srkj.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpSession;
import java.util.Map;

/**
 * com.srkj.repository
 * Created by zhangshaoxiong
 * data : 2018 11 21 11:21
 */
@Controller
public class LoginController {
    @Autowired
    private UserService userService;

    /**
     * 登录
     * @param username
     * @param password
     * @param map
     * @param session
     * @return
     * @throws Exception
     */
    @PostMapping(value = "/user/login")
    public String login(@RequestParam("username") String username,
                        @RequestParam("password") String password,
                        Map<String,Object> map, HttpSession session) throws Exception{

        Sys_user user = userService.findByUnameAndpwd(username, password);
        if (user!=null){
            session.setAttribute("loginUser",username);
            return "redirect:/main.html";
        }else {
            map.put("msg","用户名密码错误！");
            return "login";
        }
    }


    @GetMapping(value = "/user/logout")
    public String logout(Map<String,Object> map, HttpSession session) throws Exception{
        Object loginUser = session.getAttribute("loginUser");
        if (loginUser!= null){
            session.removeAttribute("loginUser");
        }
        return "login";
    }

}
