package com.srkj;

import com.srkj.util.SpringContexUtil;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

/**
 * @author yawn
 */
@SpringBootApplication
public class ActivitiDemo6SpringbootApplication {

	public static void main(String[] args) {
		ApplicationContext applicationContext = SpringApplication.run(ActivitiDemo6SpringbootApplication.class, args);
		SpringContexUtil.setApplicationContext(applicationContext);
	}
}
