package com.srkj.service;

import com.srkj.entity.Sys_user;

import java.util.List;

/**
 * com.srkj.service
 * Created by zhangshaoxiong
 * data : 2018 11 25 11:25
 */
public interface UserService {
    /**
     * 查询登录用户
     * @param userName
     * @param passWord
     * @return
     * @throws Exception
     */
    Sys_user findByUnameAndpwd(String userName,String passWord) throws Exception;

    /**
     * 查询所有
     * @return
     * @throws Exception
     */
    List<Sys_user> findAllUser() throws Exception;


    /**
     * 新增用户
     */
    void saveUser(Sys_user user) throws Exception;


    /**
     * 根据id查询
     */
    Sys_user findByUserId(long userId) throws Exception;


    /**
     * 修改user
     */
    void updateUser(Sys_user user) throws Exception;
    /**
     * 删除操作
     */
    void deleteUser(long userId) throws Exception;
}
