package com.srkj.service.impl;

import com.srkj.entity.Product;
import com.srkj.repository.ProductRepository;
import com.srkj.service.ProductService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * com.srkj.service.impl
 * Created by zhangshaoxiong
 * data : 2018 17 20 17:20
 */
@Service
public class ProductServiceImpl implements ProductService {
    @Resource
    private ProductRepository productRepository;


    @Override
    public List<Product> findAll() throws Exception {

        List<Product> products = productRepository.findAll();
        return products;
    }

    @Override
    public void saveProduce(Product product) throws Exception {
        productRepository.save(product);

    }

    @Override
    public Product findByPid(long pid) throws Exception {
        Product product = productRepository.findOne(pid);
        return product;
    }

    @Override
    public void deleteProduct(long pid) throws Exception {
        productRepository.delete(pid);
    }

}
