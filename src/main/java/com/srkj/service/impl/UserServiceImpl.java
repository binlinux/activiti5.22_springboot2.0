package com.srkj.service.impl;

import com.srkj.entity.Sys_user;
import com.srkj.repository.UserRepository;
import com.srkj.service.UserService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * com.srkj.service.impl
 * Created by zhangshaoxiong
 * data : 2018 11 27 11:27
 */
@Service
public class UserServiceImpl implements UserService{

    @Resource
    private UserRepository userRepository;

    @Override
    public Sys_user findByUnameAndpwd(String userName, String passWord) throws Exception {
        Sys_user user = userRepository.findByUsernameAndPassword(userName, passWord);
        return user;
    }

    @Override
    public List<Sys_user> findAllUser() throws Exception {
        List<Sys_user> all = userRepository.findAll();
        return all;
    }

    @Override
    public void saveUser(Sys_user user) throws Exception {
        userRepository.save(user);
    }

    @Override
    public Sys_user findByUserId(long userId) throws Exception {
        Sys_user user = userRepository.findOne(userId);
        return user;
    }

    @Override
    public void updateUser(Sys_user user) throws Exception {
        userRepository.updateUser(user.getUsername(),user.getEmail(),user.getMobile(),user.getStatus().intValue(),user.getUser_id());
    }

    @Override
    public void deleteUser(long userId) throws Exception {
        userRepository.delete(userId);
    }


}
