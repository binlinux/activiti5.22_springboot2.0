package com.srkj.service;

import com.srkj.entity.Product;

import java.util.List;

/**
 * com.srkj.service
 * Created by zhangshaoxiong
 * data : 2018 17 20 17:20
 */
public interface ProductService {
    /**
     * 查询所有
     * @return
     * @throws Exception
     */
    List<Product> findAll() throws Exception;

    /**
     * 新增product
     * @param product
     * @throws Exception
     */
    void saveProduce(Product product)throws Exception;

    /**
     * id查询
     */
    Product findByPid(long pid) throws Exception;

    /**
     * 删除
     */
    void deleteProduct(long pid) throws Exception;
}
