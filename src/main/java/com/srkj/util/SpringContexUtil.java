package com.srkj.util;

import org.springframework.context.ApplicationContext;

/**
 * zhangshaoxiong
 * com.srkj.util
 * 2018/6/3 13:23
 */
public class SpringContexUtil {
    private static ApplicationContext applicationContext;
    //获取上下文
    public static ApplicationContext getApplicationContext(){
        return applicationContext;
    }

    //设置上下文
    public static void setApplicationContext(ApplicationContext applicationContext){
        SpringContexUtil.applicationContext = applicationContext;
    }
    //通过名字获取上下文中的bean
    public static Object getBean(String name){
         return applicationContext.getBean(name);
    }
    //通过类型获取上下文的bean
    public static Object getBean(Class<?> requiredType){
        return applicationContext.getBean(requiredType);
    }
}
