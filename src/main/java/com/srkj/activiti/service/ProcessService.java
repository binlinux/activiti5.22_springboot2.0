package com.srkj.activiti.service;

import com.srkj.activiti.entity.Act_product_record;

import java.io.InputStream;

/**
 * activiti方法实现
 * zhangshaoxiong
 * com.srkj.activiti.service
 * 2018/6/2 22:40
 */
public interface ProcessService {
    /**
     * 显示图片-通过流程定义ID（不带流程跟踪）
     */
    InputStream getDiagramByProDefinitionId_noTrace(String resourceType,String processDefinitionId) throws Exception;

    /**
     * 启动流程
     * @param act_product_record
     * @return
     */
    String startProductProcess(Act_product_record act_product_record) throws Exception;

}
