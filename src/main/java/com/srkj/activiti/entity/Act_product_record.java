package com.srkj.activiti.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.io.Serializable;
import java.util.Date;

@Entity
public class Act_product_record implements Serializable{
  @Id
  @GeneratedValue
  private String id;
  private String username;
  private Long userid;
  private String dscp;
  private String proc_inst_id;
  private String status;
  private String productids;
  private Date applydate;

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getUsername() {
    return username;
  }

  public void setUsername(String username) {
    this.username = username;
  }

  public Long getUserid() {
    return userid;
  }

  public void setUserid(Long userid) {
    this.userid = userid;
  }

  public String getDscp() {
    return dscp;
  }

  public void setDscp(String dscp) {
    this.dscp = dscp;
  }

  public String getProc_inst_id() {
    return proc_inst_id;
  }

  public void setProc_inst_id(String proc_inst_id) {
    this.proc_inst_id = proc_inst_id;
  }

  public String getStatus() {
    return status;
  }

  public void setStatus(String status) {
    this.status = status;
  }

  public String getProductids() {
    return productids;
  }

  public void setProductids(String productids) {
    this.productids = productids;
  }

  public Date getApplydate() {
    return applydate;
  }

  public void setApplydate(Date applydate) {
    this.applydate = applydate;
  }
}
