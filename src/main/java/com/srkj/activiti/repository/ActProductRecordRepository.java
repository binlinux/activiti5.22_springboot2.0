package com.srkj.activiti.repository;

import com.srkj.activiti.entity.Act_product_record;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * com.srkj.activiti.repository
 * Created by zhangshaoxiong
 * data : 2018 11 23 11:23
 */
public interface ActProductRecordRepository extends JpaRepository<Act_product_record,String>{
}
