package com.srkj.activiti.controller;

import com.srkj.activiti.entity.Act_product_record;
import com.srkj.activiti.service.ProcessService;
import com.srkj.entity.Product;
import com.srkj.entity.Sys_user;
import com.srkj.service.ProductService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

/**
 * 流程实现的controller
 * zhangshaoxiong
 * com.srkj.activiti.controller
 * 2018/6/3 21:38
 */
@Controller
@RequestMapping("pruductProcess")
public class PruductProcessController {
    private static final Logger logger = Logger.getLogger(PruductProcessController.class);

    @Autowired
    private ProductService productService;
    @Autowired
    private ProcessService processService;
    /**
     * 添加商品，启动流程实例
     * @param productIds 商品ids
     * @param status 购买 0 购物车1
     * @return
     * @throws Exception
     */
    @RequestMapping("/saveshop")
    public String saveShop(String productIds, int status, HttpSession session){
        Sys_user user = (Sys_user) session.getAttribute("loginUser");
        String[] productid = productIds.split(",");
        List<Product> products = new ArrayList<>();
        try{
            for (String pid:productid) {
                Product product = productService.findByPid(Long.getLong(pid));
                products.add(product);
            }
            if (productid.length == products.size()){
                Act_product_record act_product_record = new Act_product_record();
                act_product_record.setProductids(productIds);
                act_product_record.setApplydate(new Date());
                act_product_record.setStatus("0");
                act_product_record.setUserid(user.getUser_id());
                act_product_record.setUsername(user.getUsername());
                act_product_record.setDscp("开始流程");
                String uuid = UUID.randomUUID().toString();
                act_product_record.setId(uuid);
                String ProcessInstanceId = processService.startProductProcess(act_product_record);
            }
        }catch (Exception e){
            logger.error("启动薪资调整流程失败：", e);
        }
        return "redirect:/products";
    }



}
