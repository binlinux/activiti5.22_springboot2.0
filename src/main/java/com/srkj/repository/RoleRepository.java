package com.srkj.repository;

import com.srkj.entity.Sys_role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * com.srkj.repository
 * Created by zhangshaoxiong
 * data : 2018 11 21 11:21
 */
@Repository
public interface RoleRepository extends JpaRepository<Sys_role, Long> {

}
