package com.srkj.repository;

import com.srkj.entity.Sys_role_dept;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * com.srkj.repository
 * Created by zhangshaoxiong
 * data : 2018 11 21 11:21
 */
@Repository
public interface RoleDeptRepository extends JpaRepository<Sys_role_dept, Long> {

}
