package com.srkj.repository;

import com.srkj.entity.Sys_user_role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * com.srkj.repository
 * Created by zhangshaoxiong
 * data : 2018 11 21 11:21
 */
@Repository
public interface UserRoleRepository extends JpaRepository<Sys_user_role, Long> {

}
