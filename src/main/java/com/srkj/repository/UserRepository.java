package com.srkj.repository;

import com.srkj.entity.Sys_user;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 * com.srkj.repository
 * Created by zhangshaoxiong
 * data : 2018 11 21 11:21
 */
@Repository
public interface UserRepository extends JpaRepository<Sys_user, Long> {
    /**
     * 账号密码查询
     *
     * @param userName
     * @param password
     * @return
     */
    Sys_user findByUsernameAndPassword(String userName, String password);


    /**
     * 修改user
     */
    @Transactional
    @Modifying
    @Query(value = "UPDATE sys_user user SET user.username = ?1 , user.email = ?2 " +
            ", user.mobile = ?3 , user.status = ?4 where user.user_id = ?5" , nativeQuery = true)
    void updateUser(String username,String email,String mobile,Integer status,long userId);
}
