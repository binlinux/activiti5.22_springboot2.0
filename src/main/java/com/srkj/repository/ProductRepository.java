package com.srkj.repository;

import com.srkj.entity.Product;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * com.srkj.repository
 * Created by zhangshaoxiong
 * data : 2018 17 19 17:19
 */
public interface ProductRepository extends JpaRepository<Product, Long> {

}
