package com.srkj.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
@Entity
public class Sys_oss {
  @Id
  @GeneratedValue
  private Long id;
  private String url;
  private java.sql.Timestamp create_date;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getUrl() {
    return url;
  }

  public void setUrl(String url) {
    this.url = url;
  }

  public java.sql.Timestamp getCreate_date() {
    return create_date;
  }

  public void setCreate_date(java.sql.Timestamp create_date) {
    this.create_date = create_date;
  }
}
