package com.srkj.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Sys_dept {
  @Id
  @GeneratedValue
  private Long dept_id;
  private Long parent_id;
  private String name;
  private Long order_num;
  private Long del_flag;

  public Long getDept_id() {
    return dept_id;
  }

  public void setDept_id(Long dept_id) {
    this.dept_id = dept_id;
  }

  public Long getParent_id() {
    return parent_id;
  }

  public void setParent_id(Long parent_id) {
    this.parent_id = parent_id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Long getOrder_num() {
    return order_num;
  }

  public void setOrder_num(Long order_num) {
    this.order_num = order_num;
  }

  public Long getDel_flag() {
    return del_flag;
  }

  public void setDel_flag(Long del_flag) {
    this.del_flag = del_flag;
  }
}
