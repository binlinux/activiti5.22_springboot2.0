package com.srkj.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Sys_config {
  @Id
  @GeneratedValue
  private Long id;
  private String param_key;
  private String param_value;
  private Long status;
  private String remark;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getParam_key() {
    return param_key;
  }

  public void setParam_key(String param_key) {
    this.param_key = param_key;
  }

  public String getParam_value() {
    return param_value;
  }

  public void setParam_value(String param_value) {
    this.param_value = param_value;
  }

  public Long getStatus() {
    return status;
  }

  public void setStatus(Long status) {
    this.status = status;
  }

  public String getRemark() {
    return remark;
  }

  public void setRemark(String remark) {
    this.remark = remark;
  }
}
