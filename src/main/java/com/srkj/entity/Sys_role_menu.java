package com.srkj.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
@Entity
public class Sys_role_menu {
  @Id
  @GeneratedValue
  private Long id;
  private Long role_id;
  private Long menu_id;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public Long getRole_id() {
    return role_id;
  }

  public void setRole_id(Long role_id) {
    this.role_id = role_id;
  }

  public Long getMenu_id() {
    return menu_id;
  }

  public void setMenu_id(Long menu_id) {
    this.menu_id = menu_id;
  }
}
