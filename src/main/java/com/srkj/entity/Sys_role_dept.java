package com.srkj.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
@Entity
public class Sys_role_dept {
  @Id
  @GeneratedValue
  private Long id;
  private Long role_id;
  private Long dept_id;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public Long getRole_id() {
    return role_id;
  }

  public void setRole_id(Long role_id) {
    this.role_id = role_id;
  }

  public Long getDept_id() {
    return dept_id;
  }

  public void setDept_id(Long dept_id) {
    this.dept_id = dept_id;
  }
}
