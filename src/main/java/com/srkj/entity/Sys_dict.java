package com.srkj.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Sys_dict {
  @Id
  @GeneratedValue
  private Long id;
  private String name;
  private String type;
  private String code;
  private String value;
  private Long order_num;
  private String remark;
  private Long del_flag;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }

  public String getCode() {
    return code;
  }

  public void setCode(String code) {
    this.code = code;
  }

  public String getValue() {
    return value;
  }

  public void setValue(String value) {
    this.value = value;
  }

  public Long getOrder_num() {
    return order_num;
  }

  public void setOrder_num(Long order_num) {
    this.order_num = order_num;
  }

  public String getRemark() {
    return remark;
  }

  public void setRemark(String remark) {
    this.remark = remark;
  }

  public Long getDel_flag() {
    return del_flag;
  }

  public void setDel_flag(Long del_flag) {
    this.del_flag = del_flag;
  }
}
