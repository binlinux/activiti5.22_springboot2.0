package com.srkj.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
@Entity
public class Sys_menu {
  @Id
  @GeneratedValue
  private Long menu_id;
  private Long parent_id;
  private String name;
  private String url;
  private String perms;
  private Long type;
  private String icon;
  private Long order_num;

  public Long getMenu_id() {
    return menu_id;
  }

  public void setMenu_id(Long menu_id) {
    this.menu_id = menu_id;
  }

  public Long getParent_id() {
    return parent_id;
  }

  public void setParent_id(Long parent_id) {
    this.parent_id = parent_id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getUrl() {
    return url;
  }

  public void setUrl(String url) {
    this.url = url;
  }

  public String getPerms() {
    return perms;
  }

  public void setPerms(String perms) {
    this.perms = perms;
  }

  public Long getType() {
    return type;
  }

  public void setType(Long type) {
    this.type = type;
  }

  public String getIcon() {
    return icon;
  }

  public void setIcon(String icon) {
    this.icon = icon;
  }

  public Long getOrder_num() {
    return order_num;
  }

  public void setOrder_num(Long order_num) {
    this.order_num = order_num;
  }
}
