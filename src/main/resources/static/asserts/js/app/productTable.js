$(function () {
    $('#productTable').bootstrapTable({
        url:"/findProductList",
        method:"post",
        dataType:'json',
        columns:[
            {
                field:'id',checkbox:true, title:'id',align:'center'
            },{
                field:'pname', title:'商品名称',align:'center'
            },{
                field:'pcode', title:'商品编号',align: 'center'
            },{
                field:'price',title:'价格',align:'center'
            },{
                field:'num', title:'数量',align:'center'
            },{
                field:'memo', title:'描述',align:'center'
            }
        ],
        toolbar:'#boolbar'
    });

});


function saveshop(status) {
    var products = $('#productTable').bootstrapTable('getSelections');
    var productIds = new Array();
    for (var i = 0;i<products.length;i++){
        productIds.push( products[i].pid);
    }

    $.post("/pruductProcess/saveshop",{productIds:productIds.join(),status:status},function(result){

    });


}