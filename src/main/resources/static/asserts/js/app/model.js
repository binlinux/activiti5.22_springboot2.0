var model_bootstrapTable;
$(function () {
    $('#productTable').bootstrapTable({
        url:"/processModel/listModel",
        method:"post",
        dataType:'json',
        idField:'id',
        singleSelect:true,
        columns:[
            {
                field:'ids', checkbox:true,title:'id',align:'center'
            },
            {
                field:'id',title:'id',align:'center'
            },{
                field:'key', title:'KEY',align:'center'
            },{
                field:'name', title:'名称',align: 'center'
            },{
                field:'version',title:'版本',align:'center'
            },{
                field:'createTime', title:'创建时间',align:'center'
            },{
                field:'lastUpdateTime', title:'最后更新时间',align:'center'
            },{
                field:'metaInfo', title:'元数据',align:'center'
            }
        ],
        toolbar:'#boolbar'
    });

});


function add() {
    //显示新增的dialog
    $('#myModal').modal();
}
function createModel() {
    $("#model_form").submit();
}

function delModel() {
    var data = $('#productTable').bootstrapTable('getSelections');
    var modelId;
    for(var i = 0;i<data.length;i++){
        modelId = data[i].id;
    }
    $.ajax({
        url:"/processModel/delete",
        data:{modelId:modelId},
        type:'post',
        async: false,
        success: function(result){
            $("#productTable").bootstrapTable('removeAll').bootstrapTable('refresh');
        }
    });

}
function edit(){
    var data = $('#productTable').bootstrapTable('getSelections');
    var modelId;
    for(var i = 0;i<data.length;i++){
        modelId = data[i].id;
    }
    window.open("/modeler.html?modelId=" + modelId);
    // $.get("/processModel/update",{modelId:modelId});

}


function exportModel(){
    var data = $('#productTable').bootstrapTable('getSelections');
    var modelId;
    for(var i = 0;i<data.length;i++){
        modelId = data[i].id;
    }
    // $.get("/processModel/export",{modelId:modelId});
    window.open("/processModel/export?modelId="+modelId);

}

function deploy() {
    var data = $('#productTable').bootstrapTable('getSelections');
    var modelId;
    for(var i = 0;i<data.length;i++){
        modelId = data[i].id;
    }
    $.ajax({
        url:'/processModel/deploy/',
        type: 'post',
        dataType: 'json',
        data: {modelId:modelId}
    });

}


